## Assumptions and Notes:
- I assume that every time that the Application is started (or restarted) the previous data is deleted and there is no data until a CSV is processed.
- I assume that the locale is "." for decimal separator and Time Zone GMT+2 (Europe summer)
- I assume that when loading a CSV with prices, all prices correctly formatted are processed and update (if needed) and when a price id is not on sequence the process should stop and not to process the rest of prices on the CSV.
- I assume that not all CSV can have all the prices, so it is possible to process only one price or a sequence of prices starting on any id. So that, the CSV are on sequence but the prices stored could not be on sequence.
- I assume that the request of an instrument should return a JSON response.
- In order to detected errors, I assume that the rest request should return error http codes (404, 209, etc.). More info on code. 
- There should be more test in order to test the correctness and possible error on each CSV field (format, rounding, etc.), but I have opted out because there is not much time to do it.
- On Cucumber all features and functionality should have its own Feature Files. I have opted to put all on the same file, in order to make it simple and quick.
- Probably test should be divided on more files by functionality. I have opted to put all on the same file, in order to make it simple and quick.

## Details:
Code was written using Eclipse as IDE. I have used Maven.

- Tasks 1, 2, 3. I have written Unit test (UnitTests.java file) and End to End Test using Junit vanilla (EndToEndTest.java file) and cucumber (test_scenario.feature, RunCucumberTest.java and StepDefintions.java files)
- Task 4. I have set the tests as http requests to the server and only analyzing the resulting data. 

On the tests I send CSV in order to set a context for the tests, an analyze different request and it results.

In addition to the tests performed, it should be made more test in order to test things such the resilience to the data provided, the correct format, etc… and set the tests on a more detailed manner. I have tried to use the smaller number of tests, in order to make it quick (some tests check more than one thing).

In order to run the application it should be done with the command “mvn spring-boot:run”.

In order to run the tests (unit or End to end) it should be done with the command “mvn test”.

In order to run the end-to-end tests, it is needed to launch first the spring application and after it launch the tests. It only tests the data provided and the data returned on the http requests.

I have that detected that some times it is only launched the cucumber tests when using maven test. So, in order to launch the other test, probably it will be necessary to move the cucumber test files to a temp folder and run the tests.


#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Test Several Features
	Check if prices on CSV are in secuential order


  Scenario Outline: Prices on CSV with different sequences
    Given Having a CSV on order <sequential>
    When Send CSV to <url>
    Then Validate status <status>

    Examples: 
      | sequential       | url       | status     | 
      | "non_sequential" | "postCSV" | "conflict" | # test a CSV with ids that are not on sequence.
      | "sequential"     | "postCSV" | "ok"       | # test a CSV with ids that are on sequence.
      
 Scenario Outline: Retrieve prices with different ids
    Given Having a CSV loaded
    When Send Get request for <id>
    Then Validate status <status>

    Examples: 
      | id       | status      |
      | "AAA"    | "not_found" | # test an incorrect id value
      | "110"    | "not_found" | # test an non yet loaded id
      | "107"    | "ok"        | # test an alredy loaded id

 Scenario Outline: Check adjustment is correctly applied and that the last price is the one available
    Given A loaded instrument <instrument> with id <id> with values <bid> <ask> <time> 
    When Loading instrument <instrument> with id <id> with new_values <new_bid> <new_ask> <new_time> 
    Then Validate that for instrument with id <id> adjustment is bid <adjusted_bid> and  ask <adjusted_ask>

    Examples: 
      | instrument | id    | bid       | ask       | time                      | new_bid   | new_ask   | new_time                  | adjusted_bid | adjusted_ask |
      | "GBP/USD"  | "109" | "1.2499"  | "1.2561"  | "01-06-2020 12:01:02:100" | "1.1499"  | "1.1561"  | "01-06-2020 12:01:01:100" | "1.12491"    | "1.38171"    | # first load a prices for an instrument on a CSV and latter load new prices for the same instrument but with an older time stamp than the first prices, the price retrieved and adjustemt shoould be done with first prices loaded.
      | "EUR/JPY"  | "110" | "118.61"  | "118.91"  | "01-06-2020 12:01:02:110" | "119.61"  | "119.91"  | "01-06-2020 12:01:04:110" | "107.649"    | "131.901"    | # first load a prices for an instrument on a CSV and latter load new prices for the same instrument but with a newer time stamp than the first prices, the price retrieved and adjustemt shoould be done with last prices loaded.

      
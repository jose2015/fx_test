package com.commonms.fxtest.test_cucumber;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hamcrest.Matcher;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

	private String csv_non_seq = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001\n"
			+ "108, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002";

	private String csv_seq = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001\n"
			+ "107, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002";

	private String csv_post = "";
	private HttpResponse httpResponse;

	private void request_post (String suffix) throws ClientProtocolException, IOException
	{
		HttpPost request = new HttpPost("http://localhost:8080/postCSV/");
		request.addHeader("content-type", "text/csv");
		request.setEntity(new StringEntity(csv_post));
		httpResponse = HttpClientBuilder.create().build().execute(request);
	}
	
	private void request_get (String id) throws ClientProtocolException, IOException
	{
		HttpGet request = new HttpGet( "http://localhost:8080/get/" + id );
		httpResponse = HttpClientBuilder.create().build().execute( request );
	}
	
	@Given("Having a CSV on order {string}")
	public void set_csv(String seq_order) {
		switch (seq_order) {
		case "non_sequential":
			csv_post = csv_non_seq;
			break;
		case "sequential":
			csv_post = csv_seq;
			break;
		}

	}
	
	@Given("Having a CSV loaded")
	public void load_default_csv() throws ClientProtocolException, IOException
	{
		csv_post = csv_seq;
		request_post("postCSV");
	}
	
	@Given("A loaded instrument {string} with id {string} with values {string} {string} {string}")
	public void preload_price(String instrument, String id, String bid, String ask, String time) throws ClientProtocolException, IOException
	{
		String csv = generate_csv_line(instrument,id,bid,ask,time);
		csv += "\n";
		csv_post = csv;
		request_post("postCSV");
	}

	@When("Send CSV to {string}")
	public void send_csv_post_request(String url) throws ClientProtocolException, IOException {
		request_post(url);
	}
	
	@When("Send Get request for {string}")
	public void send_id_get_request(String id) throws ClientProtocolException, IOException {
		request_get(id);
	}
	
	@When("Loading instrument {string} with id {string} with new_values {string} {string} {string}")
	public void load_new_date_for_instrument(String instrument, String id, String bid, String ask, String time) throws ClientProtocolException, IOException {
		String csv = generate_csv_line(instrument,id,bid,ask,time);
		csv += "\n";
		csv_post = csv;
		request_post("postCSV");
	}

	@Then("Validate status {string}")
	public void val_status(String code) {
		assertThat(httpResponse.getStatusLine().getStatusCode(), resolve_matcher_from_code(code));
	}
	
	@Then("Validate that for instrument with id {string} adjustment is bid {string} and  ask {string}")
	public void val_status(String id, String adjusted_bid, String adjusted_ask) throws ClientProtocolException, IOException {
		request_get(id);
	    String json = new BufferedReader(
	    	      new InputStreamReader(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8))
	    	        .lines()
	    	        .collect(Collectors.joining("\n"));
	    assertThat(getJSONValue("bid", json), is(adjusted_bid));
	    assertThat(getJSONValue("ask", json), is(adjusted_ask));
		System.out.println("JAG06 json: "+json);
	}	

	private Matcher<Integer> resolve_matcher_from_code(String code) {
		Matcher<Integer> matcher = null;
		switch (code) {
		case "conflict":
			matcher = is(equalTo(HttpStatus.SC_CONFLICT));
			break;

		case "ok":
			matcher = either(is(HttpStatus.SC_ACCEPTED)).or(is(HttpStatus.SC_OK));
			break;

		case "not_found":
			matcher = is(HttpStatus.SC_NOT_FOUND) ;
			break;
		}
		return matcher;
	}
	
	private String generate_csv_line (String instrument, String id, String bid, String ask, String time)
	{
		return id+","+instrument+","+bid+","+ask+","+time;
	}
	
	// it is not the prettiest way to get a value from a JSON, but it is a easy way without using external dependencies
	private String getJSONValue(String id, String json)
	{
		String val = null;
		String [] elems = json.replace('{', ' ').replace('}', ' ').trim().split(",");
		for (int i = 0; i < elems.length; i++) {
			String aux = "\""+id+"\":";
			if (elems[i].contains(aux))
			{
				val = elems[i].replace(aux, " ").replaceAll("\"", " ").trim();
			}
		}
		return val;
	}
}
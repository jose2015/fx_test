package com.commonms.fxtest.test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class EndToEndTest {
	@Test
	@Order(2)
	public void givenRequestWithNoCorrectID_whenRequestIsExecuted_thenResponseHTTPNotFound()
	  throws IOException {
		
	    // Given
	    String name = "AAA1";
	    HttpGet request = new HttpGet( "http://localhost:8080/get/" + name );

	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

	    // Then
	    assertEquals(httpResponse.getStatusLine().getStatusCode(), HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	@Order(3)
	public void givenRequestWithNonExitingID_whenRequestIsExecuted_thenResponseHTTPNotFound()
	  throws IOException {
		
	    // Given
	    String name = "110";
	    HttpGet request = new HttpGet( "http://localhost:8080/get/" + name );

	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

	    // Then
	    assertThat(httpResponse.getStatusLine().getStatusCode(), is(HttpStatus.SC_NOT_FOUND) );
	    
	}

	@Test
	@Order(1)
	public void givenRequestWithCorrectCSV_whenRequestIsExecuted_thenResponseHTTPOKOrACCEPTED()
	  throws IOException {
		
	    // Given
	    String csv = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001\n"
	    		+ "107, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002";
	    HttpPost request = new HttpPost( "http://localhost:8080/postCSV/" );
	    request.addHeader("content-type", "text/csv");
	    request.setEntity(new StringEntity(csv));
	    
	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

	    // Then
	    assertThat(httpResponse.getStatusLine().getStatusCode(), either(is(HttpStatus.SC_ACCEPTED)).or(is(HttpStatus.SC_OK)));
	    
	}
	
	@Test
	@Order(4)
	public void givenRequesNosSequentialIDsCSV_whenRequestIsExecuted_thenResponseHTTPConflict()
	  throws IOException {
		
	    // Given
	    String csv = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001\n"
	    		+ "108, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002";
	    HttpPost request = new HttpPost( "http://localhost:8080/postCSV/" );
	    request.addHeader("content-type", "text/csv");
	    request.setEntity(new StringEntity(csv));
	    
	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

	    // Then
	    assertThat(httpResponse.getStatusLine().getStatusCode(), is(HttpStatus.SC_CONFLICT));
	    
	}
	
	@Test
	@Order(5)
	public void givenRequestforAnExistingPrice_whenRequestIsExecuted_thenResponseHTTPOKOrACCEPTED()
	  throws IOException {
		
		// Given
	    String name = "107";
	    HttpGet request = new HttpGet( "http://localhost:8080/get/" + name );
	    
	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

	    // Then
	    assertThat(httpResponse.getStatusLine().getStatusCode(), Matchers.either(Matchers.is(HttpStatus.SC_ACCEPTED)).or(Matchers.is(HttpStatus.SC_OK)));
	    
	}
	
	@Test
	@Order(6)
	public void givenAlreadyLoadedData_whenNewDataLoadedWithOlderDates_thenPricesAreAlreadyStoredAndAdjusted()
	  throws IOException {
		
		 // Given
		// tests run in order, date is already loaded.
	    
	    // When
		String csv = "107, EUR/JPY, 118.60,118.90,01-06-2020 11:01:02:002";
	    HttpPost request_post = new HttpPost( "http://localhost:8080/postCSV/" );
	    request_post.addHeader("content-type", "text/csv");
	    request_post.setEntity(new StringEntity(csv));
	    HttpClientBuilder.create().build().execute( request_post );
	    
	    // Then
	    String name = "107";
	    HttpGet request_get = new HttpGet( "http://localhost:8080/get/" + name );
	    HttpResponse httpResponse_get = HttpClientBuilder.create().build().execute( request_get );
	    String json = new BufferedReader(
	    	      new InputStreamReader(httpResponse_get.getEntity().getContent(), StandardCharsets.UTF_8))
	    	        .lines()
	    	        .collect(Collectors.joining("\n"));
	    
	    MathContext mathContextPrice = new MathContext(4, RoundingMode.HALF_EVEN);
	    
	    BigDecimal bid_adj_actual = new BigDecimal(getJSONValue("bid", json), mathContextPrice);
	    BigDecimal bid_adj_expected = new BigDecimal("119.60", mathContextPrice);
	    bid_adj_expected = bid_adj_expected.multiply(new BigDecimal ("0.9", mathContextPrice));
	    assertEquals(bid_adj_expected, bid_adj_actual);
	    
	    BigDecimal ask_adj_actual = new BigDecimal(getJSONValue("ask", json), mathContextPrice);
	    BigDecimal ask_adj_expected = new BigDecimal("119.90", mathContextPrice);
	    ask_adj_expected = ask_adj_expected.multiply(new BigDecimal ("1.1", mathContextPrice));
	    assertEquals(ask_adj_expected, ask_adj_actual);

	}
	
	// it is not the prettiest way to get a value from a JSON, but it is a easy way without using external dependencies
	public String getJSONValue(String id, String json)
	{
		String val = null;
		String [] elems = json.replace('{', ' ').replace('}', ' ').trim().split(",");
		for (int i = 0; i < elems.length; i++) {
			String aux = "\""+id+"\":";
			if (elems[i].contains(aux))
			{
				val = elems[i].replace(aux, " ").replaceAll("\"", " ").trim();
			}
		}
		return val;
	}
}

package com.commonms.fxtest.test;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.commonms.fxtest.controller.exceptions.PricesNotInSequence;
import com.commonms.fxtest.model.Price;
import com.commonms.fxtest.service.PricesService;

@ExtendWith(MockitoExtension.class)
@WebMvcTest //(PriceController.class)
class UnitTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private PricesService service;
	
	private PricesService priceService = new PricesService();
	

	@Test
	void givenCSVDataCorretFormat_whenSentCSV_thenReturnOk() throws Exception {

		// Given
		String csv = "106, EUR/USD, 1.1000,1.2000,01-06-2020 11:01:01:001\n"
				+ "108, EUR/JPY, 120.60,120.90,01-06-2020 12:01:03:002";
		
		// When
		ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/postCSV").contentType("text/csv").content(csv));
		
		// Then
		assertThat( (Integer) HttpServletResponse.SC_OK, equalTo( result.andReturn().getResponse().getStatus())) ;
	

	}
	
	@Test
	void givenAnyData_whenPricesNotInOrder_thenReturnException() throws Exception {

		// Given
		MathContext mathContextPrice = new MathContext(4, RoundingMode.HALF_EVEN);
		DateTimeFormatter datetimeFromatter = Price.DATE_FORMAT;
		Price[] prices = new Price[2];
		prices[0] = new Price(106, "EUR/USD", new BigDecimal("1.1000", mathContextPrice), new BigDecimal("1.2000", mathContextPrice), LocalDateTime.parse("01-06-2020 11:01:01:001", datetimeFromatter));
		prices[1] = new Price(108, "EUR/JPY", new BigDecimal("120.60", mathContextPrice), new BigDecimal("120.90", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:03:002", datetimeFromatter));
		
		// When
		PricesNotInSequence t = assertThrows( PricesNotInSequence.class, () -> priceService.addPrice(prices) );
		
		// Then
		assertEquals(t.getClass().toString(), PricesNotInSequence.class.toString());

	}
	
	@Test
	void givenAnyData_whenPricesInOrder_thenNotReturnException() throws Exception {

		// Given
		MathContext mathContextPrice = new MathContext(4, RoundingMode.HALF_EVEN);
		DateTimeFormatter datetimeFromatter = Price.DATE_FORMAT;
		Price[] prices = new Price[2];
		prices[0] = new Price(106, "EUR/USD", new BigDecimal("1.1000", mathContextPrice), new BigDecimal("1.2000", mathContextPrice), LocalDateTime.parse("01-06-2020 11:01:01:001", datetimeFromatter));
		prices[1] = new Price(107, "EUR/JPY", new BigDecimal("120.60", mathContextPrice), new BigDecimal("120.90", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:03:002", datetimeFromatter));
		
		// Then 
		assertDoesNotThrow(() -> priceService.addPrice(prices) );
	}

	@Test
	void givenAlreadyLoadedData_whenLooadingOlderPrices_thenPriceStoredIsTheNewerWithCorrectAdjustments() throws Exception {

		// Given
		MathContext mathContextPrice = new MathContext(4, RoundingMode.HALF_EVEN);
		DateTimeFormatter datetimeFromatter = Price.DATE_FORMAT;
		Price[] prices_1 = new Price[2];
		prices_1[0] = new Price(109, "GBP/USD", new BigDecimal("1.2499", mathContextPrice), new BigDecimal("1.2561", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:02:100", datetimeFromatter));
		prices_1[1] = new Price(110, "EUR/JPY", new BigDecimal("118.61", mathContextPrice), new BigDecimal("118.91", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:02:110", datetimeFromatter));
		priceService.addPrice(prices_1);
		
		// When
		Price[] prices_2 = new Price[2];
		prices_2[0] = new Price(109, "GBP/USD", new BigDecimal("1.1499", mathContextPrice), new BigDecimal("1.1561", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:01:100", datetimeFromatter));
		prices_2[1] = new Price(110, "EUR/JPY", new BigDecimal("119.61", mathContextPrice), new BigDecimal("119.91", mathContextPrice), LocalDateTime.parse("01-06-2020 12:01:04:110", datetimeFromatter));
		priceService.addPrice(prices_2);
		
		//Then
		Price p_109 = priceService.getPrice(109);
		BigDecimal p_109_adjust_bid = new BigDecimal("1.12491", mathContextPrice);
		assertEquals(p_109.getBid(), p_109_adjust_bid );
		BigDecimal p_109_adjust_ask = new BigDecimal("1.38171", mathContextPrice);
		assertEquals(p_109.getAsk(), p_109_adjust_ask );

		Price p_110 = priceService.getPrice(110);
		BigDecimal p_110_adjust_bid = new BigDecimal("107.649", mathContextPrice);
		assertEquals(p_110.getBid(), p_110_adjust_bid );
		BigDecimal p_110_adjust_ask = new BigDecimal("131.901", mathContextPrice);
		assertEquals(p_110.getAsk(), p_110_adjust_ask );
		
	}
}

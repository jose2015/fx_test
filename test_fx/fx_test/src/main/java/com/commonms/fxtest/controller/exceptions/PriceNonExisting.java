package com.commonms.fxtest.controller.exceptions;

public class PriceNonExisting extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4571910324884492815L;

	public PriceNonExisting() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PriceNonExisting(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PriceNonExisting(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PriceNonExisting(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PriceNonExisting(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

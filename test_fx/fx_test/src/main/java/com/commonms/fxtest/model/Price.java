package com.commonms.fxtest.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Price {
	
	public static final DateTimeFormatter DATE_FORMAT =  DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");
	private long id;
	private String instrument_name;
	private BigDecimal bid;
	private BigDecimal ask;
	@JsonFormat(pattern="dd-MM-yyyy HH:mm:ss:SSS")
	private LocalDateTime time;
	
	public Price(long id, String instrument_name, BigDecimal bid, BigDecimal ask, LocalDateTime time) {
		super();
		this.id = id;
		this.instrument_name = instrument_name;
		this.bid = bid;
		this.ask = ask;
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInstrument_name() {
		return instrument_name;
	}

	public void setInstrument_name(String instrument_name) {
		this.instrument_name = instrument_name;
	}

	public BigDecimal getBid() {
		return bid;
	}

	public void setBid(BigDecimal bid) {
		this.bid = bid;
	}

	public BigDecimal getAsk() {
		return ask;
	}

	public void setAsk(BigDecimal ask) {
		this.ask = ask;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Price [id=" + id + ", instrument_name=" + instrument_name + ", bid=" + bid + ", ask=" + ask + ", time="
				+ time + "]";
	}
	
	
	
	
}

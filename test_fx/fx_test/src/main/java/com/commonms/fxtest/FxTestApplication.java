package com.commonms.fxtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FxTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FxTestApplication.class, args);
	}
	
}

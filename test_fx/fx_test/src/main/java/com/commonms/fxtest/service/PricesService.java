package com.commonms.fxtest.service;

import java.math.BigDecimal;
import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.commonms.fxtest.controller.exceptions.PriceNonExisting;
import com.commonms.fxtest.controller.exceptions.PricesNotInSequence;
import com.commonms.fxtest.model.Price;

@Service
@Scope("singleton")
public class PricesService {
	
	private static HashMap<Long, Price> pricesMap = new HashMap<Long, Price>();
	private static BigDecimal bidAdjust = new BigDecimal("0.90");
	private static BigDecimal askAdjust = new BigDecimal("1.10");
	
	
	
	public synchronized void addPrice(Price... prices) throws PricesNotInSequence
	{
		long idCount = -1;
		for (int i = 0; i < prices.length; i++) {
			
			Price p = prices[i];
			
			if (idCount < 0)
				idCount = p.getId();
			else if ((idCount + 1) != p.getId())
				throw new PricesNotInSequence("Price " + idCount + " is not sequential with " + p.getId());
			
			// adjust prices
			BigDecimal bid = p.getBid();
			bid = bid.multiply(bidAdjust);
			
			BigDecimal ask = p.getAsk();
			ask = ask.multiply(askAdjust);
			
			long id = p.getId();
			Price price_aux = pricesMap.get(id);
			
			if (price_aux != null)
			{
				// if price stored is older do nothing
				if ( price_aux.getTime().isAfter(p.getTime()) )
					return;
			} 
			
			pricesMap.remove(id);
			pricesMap.put(id, p);
		}
		

	}
	
	public synchronized Price getPrice (long id) throws PriceNonExisting
	{
		
		Price p = pricesMap.get(id);		
		if (p == null)
			throw new PriceNonExisting("Price id "+id+" does not exist");
		
		return p;
		
	}
	
	public synchronized Price [] getAllPrices()
	{
		
			int lenght = pricesMap.values().size();
			int i = 0;
			Price [] list = new Price [lenght];
			for (Price p : pricesMap.values())
			{
				list[i] = p;
				i++;
			}
			return list;
		
	}
	

}

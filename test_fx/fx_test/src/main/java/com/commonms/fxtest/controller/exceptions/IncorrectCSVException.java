package com.commonms.fxtest.controller.exceptions;

public class IncorrectCSVException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3868955874116793208L;

	public IncorrectCSVException() {
		super();
	}

	public IncorrectCSVException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IncorrectCSVException(String message, Throwable cause) {
		super(message, cause);
	}

	public IncorrectCSVException(String message) {
		super(message);
	}

	public IncorrectCSVException(Throwable cause) {
		super(cause);
	}

	
}

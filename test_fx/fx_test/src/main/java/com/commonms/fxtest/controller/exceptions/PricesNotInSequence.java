package com.commonms.fxtest.controller.exceptions;

public class PricesNotInSequence extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8528197521066912732L;

	public PricesNotInSequence() {
		super();

	}

	public PricesNotInSequence(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public PricesNotInSequence(String message, Throwable cause) {
		super(message, cause);

	}

	public PricesNotInSequence(String message) {
		super(message);

	}

	public PricesNotInSequence(Throwable cause) {
		super(cause);

	}

}

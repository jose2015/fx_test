package com.commonms.fxtest.controller;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.commonms.fxtest.controller.exceptions.IncorrectCSVException;
import com.commonms.fxtest.controller.exceptions.PriceNonExisting;
import com.commonms.fxtest.controller.exceptions.PricesNotInSequence;
import com.commonms.fxtest.model.Price;
import com.commonms.fxtest.service.PricesService;

@RestController
public class PriceController {

	@Autowired
	PricesService service;

	private static DateTimeFormatter datetimeFromatter = Price.DATE_FORMAT;
	// I will assume that the prices values have always 4 decimals places and use
	// BigDecimals to represent the values.
	private static MathContext mathContextPrice = new MathContext(4, RoundingMode.HALF_EVEN);

	// I will try to keep the code as simple as posible. Because, I assume that
	// the objetive of the paper test is to check skills about end to end testing.
	// The Spring service may not be the most elegant and cannonic. Also I will try
	// to
	// avoid using other frameworks (for CSV processing, etc) in order to keep it
	// simple.

	@GetMapping("/get/{id}")
	public Price getID(@PathVariable long id) throws PriceNonExisting {

		return service.getPrice(id);
	}

	@PostMapping("/postCSV")
	public void postCSV(@RequestBody String csv) throws IncorrectCSVException, PricesNotInSequence {

		String[] prices = csv.split("\n\r|\n");

		Price[] prices_list = new Price[prices.length];
		for (int i = 0; i < prices.length; i++) {
			String line = prices[i];
			if (line.isBlank())
				continue;

			try {
				String[] elems = line.split(",");

				long id = Long.parseLong(elems[0].trim());

				// get data as BigDecimal
				BigDecimal bid = new BigDecimal(elems[2].trim(), mathContextPrice);
				BigDecimal ask = new BigDecimal(elems[3].trim(), mathContextPrice);

				// I am going to assume that dates on CSV are local times.
				LocalDateTime datetime = LocalDateTime.parse(elems[4].trim(), datetimeFromatter);

				Price p = new Price(id, elems[1].trim(), bid, ask, datetime);

				prices_list[i] = p;
				
			} catch (Exception e) {
				throw new IncorrectCSVException("Price format is incorrect");
			}
		}
		
		// Send prices as a list. It is not the most efficient way, but it can help in the tests,
		// separating controller from the business logic.
		service.addPrice(prices_list);

	}

	// I am going translate possible exception as HttPStatus responses,
	// so that it is easy to assert on tests.
	
	// not found for ID incorrect format.
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "ID is not a number")
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public void idNotNumber() {
	}

	// not found for ID not stored.
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "ID not existis")
	@ExceptionHandler(PriceNonExisting.class)
	public void idNotExists() {
	}

	// Bad Request for incorrect format on CSV.
	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "CSV format is incorrect or some price format is incorrect ")
	@ExceptionHandler(IncorrectCSVException.class)
	public void incorrectCSV() {
	}

	// Conflict for when some of the Ids on CSV are not in order.
	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "CSV format is incorrect, prices are not in order ")
	@ExceptionHandler(PricesNotInSequence.class)
	public void pricesNotInOrderOnCSV() {
	}

}
